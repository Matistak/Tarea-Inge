<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 4</title>
</head>
<body>
    <?php 
        session_start();
        $_SESSION['nombre'] = 'Mathias';
        $_SESSION['apellido'] = 'Lugo';
        $_SESSION['matricula'] = 'CO6433';

        echo $_SESSION['nombre']."<br>";
        echo $_SESSION['apellido']."<br>";
        echo $_SESSION['matricula']."<br>";   
    ?>

    <a href="./tema4_1.php">Pasar datos a la otra pagina</a>
</body>
</html>