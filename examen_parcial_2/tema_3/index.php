<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Examen Parcial Tema 3</title>
</head>
    <body>
        <?php 
/*      Implementar un script PHP que haga lo siguiente:
        • Crear una función que genere códigos de 4 letras (A-Z) aleatorias.
        • Crear un array de 500 elementos cuyo índice sea numérico y cuyo valor sean 
        los códigos de 4 letras generados con la función anterior.
        • Imprimir ese array utilizando la estructura foreach. Se debe imprimir de 
        manera tabular indicando índice y valor del array generado aleatoriamente. */
            function cuatroLetras(){
                $letra1 = chr(mt_rand(65,90));
                $letra2 = chr(mt_rand(65,90));
                $letra3 = chr(mt_rand(65,90));
                $letra4 = chr(mt_rand(65,90));

                $cuatroLetras = [$letra1, $letra2, $letra3, $letra4];
                return $cuatroLetras;
            }

            function arrayCuatroLetras(){
                $arrayCuatroLetras=[];
                for ($i=0; $i<500; $i++){
                    array_push($arrayCuatroLetras, cuatroLetras());
                }

                $i = 0; //se vuelve a cerar el indice

                echo <<<HTML
                <table>
                   <tr>
                       <td style='border: 1px solid black'> Indice </td>
                       <td style='border: 1px solid black'> Letra 1 </td>
                       <td style='border: 1px solid black'> Letra 2 </td>
                       <td style='border: 1px solid black'> Letra 3 </td>
                       <td style='border: 1px solid black'> Letra 4 </td>
                   </tr>
                HTML;
                foreach ( $arrayCuatroLetras as $a) {
                    echo <<<HTML
                        <tr>
                            <td style='border: 1px solid black'>$i</td>
                            <td style='border: 1px solid black'>$a[0]</td>
                            <td style='border: 1px solid black'>$a[1]</td>
                            <td style='border: 1px solid black'>$a[2]</td>
                            <td style='border: 1px solid black'>$a[3]</td>
                        </tr>
                    HTML;
                    
                    $i++;
                }
                echo "</table>";

            }
            
            arrayCuatroLetras();
        ?>
    </body>
</html>

