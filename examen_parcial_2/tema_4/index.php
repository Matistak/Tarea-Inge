<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Examen Parcial Tema 4</title>
</head>
    <body>
        <?php 
/*           Crear una Base de datos llamada examen. Esta base de datos tendrá una tabla llamada 
            alumnos que debe tener cuatro campos: id, nombre, apellido, edad (esto lo hacen de manera 
            manual utilizando el pgAdmin o un cliente similar o creando un script de SQL) (1 punto)
            Implementar un script PHP que haga lo siguiente:
            • Inserte en la tabla alumnos creada anteriormente: 12 alumnos con valores válidos 
            cualesquiera (1 punto)
            • Visualice de manera tabular los 12 alumnos creados en al paso previo (1 punto).
            • Crear una función que retorne el nombre y el apellido del alumno con mayor edad y el 
            nombre y apellido del alumno con menor edad (si hay edades iguales se elige uno al 
            azar) (1 punto). */
            include_once("connection.php");
            $a = new CConexion;
            $a->ConexionBD();
        ?>
    </body>
</html>