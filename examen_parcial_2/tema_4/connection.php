<?php 
    class CConexion {
        function ConexionBD(){       
            $cn = new PDO('pgsql:host=localhost;dbname=examen;','postgres','123');

            if($cn){
                echo "Conectado <br>";
            } else {
                echo "error";
                exit;
            }
            
            $query = "INSERT INTO alumnos 
            VALUES 
            (1,'Mathias', 'Lugo', 34),
            (2,'Roberto', 'Lopez', 34),
            (3,'Maria', 'Gozalez', 23),
            (4,'Lucia', 'Lugo', 34),
            (5,'Juan', 'Lopez', 67),
            (6,'Pablo', 'Gozalez', 45),
            (7,'Sebas', 'Lugo', 89),
            (8,'Messi', 'Lopez', 23),
            (9,'Ronaldo', 'Gozalez', 12),
            (10,'Luis', 'Lopez', 78),
            (11,'Renato', 'Lugo', 52),
            (12,'Mathias', 'Gozalez', 15)";
  
            $sql = $cn->prepare($query);
            $sql->execute();

            $query = "select a.nombre, a.apellido, a.edad from alumnos a";
            $sql = $cn->prepare($query);
            $sql->execute();
            $resultado = $sql->fetchAll();

            echo <<<HTML
             <table>
                <tr>
                    <td style='border: 1px solid black'> Nombre </td>
                    <td style='border: 1px solid black'> Apellido </td>
                    <td style='border: 1px solid black'> Edad </td>
            HTML;

            foreach ($resultado as $row){
                echo <<<HTML
                    <tr>
                        <td style='border: 1px solid black'> $row[0] </td>
                        <td style='border: 1px solid black'> $row[1] </td>
                        <td style='border: 1px solid black'> $row[2] </td>
                HTML;
            }

            echo "</table>";
        }
    }
?>