<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Septimo Ejercicio</title>
</head>
    <body>
        <?php 
            $palindromo = 'oso';
            strtolower($palindromo);
            $arrayReves = [];
            $contador = 0;

            for($i=(strlen($palindromo)-1); $i>=0; $i--){
                array_push( $arrayReves , $palindromo[$i] );
            }
            for($i=0; $i<strlen($palindromo); $i++){
                if($palindromo[$i] == $arrayReves[$i]){
                    $contador++;
                }
            }
            
            if($contador == strlen($palindromo)){
                echo "Palindromo";
            } else {
                echo "Palindromon't";
            }
        ?>
    </body>
</html>