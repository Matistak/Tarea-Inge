<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Decimo Segundo ejercicio</title>
</head>
    <body>
        <?php 
            $cadenas = ['hola','amigo','perro','soluciones','autos'];

            $primeraCadena = 'autos';

            if(in_array($primeraCadena, $cadenas)){
                echo 'Ya existe';
            } else {
                echo 'Es nuevo';
                array_push($cadenas, $primeraCadena);
            }
        
            echo '<br>';
            print_r($cadenas);
        ?>
    </body>
</html>