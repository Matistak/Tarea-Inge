<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Segundo Ejercicio</title>
</head>
<body>
    <!-- Hacer un script PHP que imprime la siguiente información:
    • Versión de PHP utilizada.
    • El id de la versión de PHP.
    • El valor máximo soportado para enteros para esa versión.
    • Tamaño máximo del nombre de un archivo.
    • Versión del Sistema Operativo.
    • Símbolo correcto de 'Fin De Línea' para la plataforma en uso.
    • El include path por defecto. -->

    <?php 
        echo 'PHP version: ' .  PHP_VERSION;
        echo '<br>';
        echo 'PHP id version: ' .  PHP_VERSION_ID; 
        echo '<br>';
        echo 'PHP valor maximo de enteros: ' .  PHP_INT_MAX; 
        echo '<br>';
        echo 'PHP tamaño maximo del nombre del archivo: ' .  'filesize()'; 
        echo '<br>';
        echo 'PHP versión del Sistema Operativo: ' .  php_uname(); 
        echo '<br>';
        echo 'PHP símbolo correcto de "Fin De Línea" para la plataforma en uso: ' .  PHP_EOL; 
        echo '<br>';
        echo 'PHP el include path por defecto: ' .  'set_include_path()'; 
    ?>
</body>
</html>