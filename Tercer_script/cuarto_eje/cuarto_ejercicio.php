<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cuarto Ejercicio</title>
</head>
<body>
    <?php
    /* Hacer un script PHP que haga lo siguiente:
        • Declarar 3 variable con valores enteros aleatorios (se debe generar aleatoriamente estos
        valores entre 50 y 900) – Se debe usar la función mt_srand y mt_rand.
        • Ordenar de mayor a menor los valores de estas variables.
        • Imprimir en pantalla la secuencia de números ordenadas, los números deben estar
        separados por espacios en blanco.
        • El número mayor debe estar en color verde y el número más pequeño debe estar en color
        rojo. */

        $a = mt_rand(50, 900);
        $b = mt_rand(50, 900);
        $c = mt_rand(50, 900);
        
        $num = array ($a, $b, $c);
        print_r($num);
        rsort($num);
        print_r($num);

        echo "<div style='color: green;'>";
        echo $num[0];
        echo "</div>";

        echo "<br>";

        echo "<div>";
        echo $num[1];
        echo "</div>";

        echo "<br>";

        echo "<div style='color: red;'>";
        echo $num[2];
        echo "</div>";
    ?>
</body>
</html>