<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Primer Ejercicio</title>
</head>
<body>
    <!-- Hacer un script PHP que realice el siguiente cálculo
    x = ((A * ¶) + B) / (C*D) - Se debe calcular e imprimir el valor de x
    Donde:
    • A es la raiz cuadrada de 2
    • ¶ es el número PI
    • B es es la raíz cúbica de 3
    • C es la constante de Euler
    • D es la constante e
    Observación: Utilizar las constantes matemáticas definidas den la extensión math de PHP -->
    <?php 
        $A = 2; 
        $¶ = pi(); 
        $B = pow(3 , 1/3);
        $C = M_EULER;
        $D =  exp($A);
        $x = (($A * $¶) + $B) / ($C*$D);

        print_r($x);
    ?>
</body>
</html>