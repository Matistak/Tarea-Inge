<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tercer Ejercicio</title>
    </head>
    <body>
        <?php 
            const CONS_NUM =  2;
            $indice = 1;
            $N = 156;

            echo "<table text-align:center; border=5>";
            echo "<tr>"; 
                echo "<th>Tabla multiplo de 2 </th>";
            echo "</tr>";

            echo "<tr>";
            for ($indice=0; $indice <= $N; $indice++) { 
                if(($indice % CONS_NUM) == 0){
                    echo "<td>";
                    echo $indice;
                    echo "</td>";
                }
            }
            echo "</tr>";

            echo "</table>";
        ?>
    </body>
</html>
