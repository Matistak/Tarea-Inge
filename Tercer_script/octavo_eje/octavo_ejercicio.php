<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Octavo Ejercicio</title>
</head>
    <body>
        <?php 
/*      Hacer un script en PHP que genere una matriz de dimensión n*m con números aleatorios. Las
        dimensiones n (filas) y m (columnas) son variables que debe definir el alumno. La matriz generada
        se debe imprimir en pantalla de manera tabular.
        Obsevación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */
            $columna = mt_rand(0,99);
            $filas = mt_rand(0,99);
            $count = 0;

            echo '<table>';
                echo '<tr>';
                for($i=0; $i<$columna; $i++){
                    echo <<<HTML
                            <th style='border: 1px solid black; background-color: yellow;'>$i</th>
                    HTML;
                }
                echo '</tr>';

                echo '<tr>';
                for($x=0; $x<$filas; $x++){
                    $count++;
                    echo <<<HTML
                            <td style='border: 1px solid black'>$x</td>
                    HTML;
                    if($count == $columna){
                        echo '<tr>';
                        $count = 0;
                    }
                }
                echo '</tr>';
            echo "</table>";  
        ?>
    </body>
</html>