<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cuarto Ejercicio</title>
</head>
<body>
    <?php 
        $variable = 24.5;
    ?>
    <h1><?php print(gettype($variable)) ?></h1>
    <br>
    <?php 
        $variable = "HOLA";
    ?>
    <h1><?php print(gettype($variable)) ?></h1>
    <br>
    <?php 
        $variable = 8;
    ?>
    <h1><?php echo var_dump($variable) ?></h1>
</body>
</html>