<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sexto Ejercicio</title>
</head>
    <body>
        <?php 
    /*      Hacer un script PHP el cual utilice el operador ternario de PHP para realizar lo siguiente:
            • Se deben declarar tres variables y le asignan valores enteros aleatorios (los valores deben
            estar entre 99 y 999). Las variables serán $a, $b y $c
            • Si la expresión $a*3 > $b+$c se debe imprimir que la expresión $a*3 es mayor que la
            expresión $b+$c
            • Si la expresión $a*3 <= $b+$c se debe imprimir que la expresión $b+$c es mayor o igual
            que la expresión $a*3 */

            $a = rand(99,999);
            $b = rand(99,999);
            $c = rand(99,999);
        ?>
        
        <?php if ($a*3 > $b + $c) { ?>
            <h1><?php echo 'la expresión $a*3 es mayor que la expresión $b+$c'?></h1>
        <?php } else if ($a*3 <= $b + $c) { ?>
            <h1><?php echo 'la expresión $b+$c es mayor o igual que la expresión $a*3'?></h1>
        <?php } ?>


    </body>
</html>