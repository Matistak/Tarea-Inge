<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quinto Ejercicio</title>
</head>
<body>
    <?php 
        $multiplicando = 9;
        $multiplicador = 0;
        $tabla = 9;

        echo "<table text-align:center; border=5>";
        echo "<tr>"; 
            echo "<th>Tabla del $tabla </th>";
        echo "</tr>";

        echo "<tr>";
        for ($multiplicador=0; $multiplicador <= 9 ; $multiplicador++) { 
            if(($multiplicador % 2) == 0){
                echo "<td style ='background-color: white;'>$multiplicando X $multiplicador =";
                echo ($multiplicando * $multiplicador);
                echo "</td>";
            } else {
                echo "<td style ='background-color: grey;'>$multiplicando X $multiplicador =";
                echo ($multiplicando * $multiplicador);
                echo "</td>";
            }
        }
        echo "</tr>";

        echo "</table>";
    ?>
</body>
</html>