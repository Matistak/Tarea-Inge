<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Septimo Ejercicio</title>
</head>
<body>
<?php
    $x = rand(0,30);
    $y = rand(0,30);
    $a = rand(0,30);

    $total = $x+$y+$a;
    switch (true) {
        case ( ($total <= 59) ):
            echo "Nota 1";
            break;
        case ( ($total >= 60) && ($total <= 69) ):
            echo "Nota 2";
            break;
        case ( ($total >= 70) && ($total <= 79) ):
            echo "Nota 3";
            break;
        case ( ($total >= 80) && ($total <= 89) ):
            echo "Nota 4";
            break;
        case ( ($total >= 90) && ($total <= 100) ):
            echo "Nota 5";
            break;
    }
?>
</body>
</html>