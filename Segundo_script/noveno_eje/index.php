<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Noveno Ejercicio</title>
</head>
    <body>
        <?php 
/*          Hacer un script en PHP que genere números aleatorios hasta que el número generado sea divisible
            por 983. Cuando ocurra esta condición imprimir un mensaje.
            Se debe usar un ciclo infinito */

            $a=rand(0,99999);
            while( ($a % 983) != 0){
                $a=rand(0,99999);
            }

            echo "Este numero es divisible por 983: ".$a;
        ?>
    </body>
</html>