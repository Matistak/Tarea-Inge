<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Segundo ejercicio de Inge</title>
</head>
<body>
    <div class="centerTable">
        <?php
            $i = 0; //index

            class Producto
            {
                public $nombre;
                public $cantidad;
                public $precio;
            }

            $productos = array();

            $productos[0] = new Producto();
            $productos[0]->nombre = 'Coca Cola';
            $productos[0]->cantidad = '100';
            $productos[0]->precio = '4.500';
            
            $productos[1] = new Producto();
            $productos[1]->nombre = 'Pepsi';
            $productos[1]->cantidad = '30';
            $productos[1]->precio = '4.800';

            $productos[2] = new Producto();
            $productos[2]->nombre = 'Sprite';
            $productos[2]->cantidad = '20';
            $productos[2]->precio = '4.500';

            $productos[3] = new Producto();
            $productos[3]->nombre = 'Guarana';
            $productos[3]->cantidad = '200';
            $productos[3]->precio = '4.500';

            $productos[4] = new Producto();
            $productos[4]->nombre = 'Seven Up';
            $productos[4]->cantidad = '24';
            $productos[4]->precio = '4.800';

            $productos[5] = new Producto();
            $productos[5]->nombre = 'Mirinda Naranja';
            $productos[5]->cantidad = '56';
            $productos[5]->precio = '4.800';

            $productos[6] = new Producto();
            $productos[6]->nombre = 'Mirinda Guarana';
            $productos[6]->cantidad = '89';
            $productos[6]->precio = '4.800';

            $productos[7] = new Producto();
            $productos[7]->nombre = 'Fanta Naranja';
            $productos[7]->cantidad = '10';
            $productos[7]->precio = '4.500';

            $productos[8] = new Producto();
            $productos[8]->nombre = 'Fanta Piña';
            $productos[8]->cantidad = '2';
            $productos[8]->precio = '4.500';

        ?>
        <table>
        <h2>Productos</h2>
        <tr>
            <th class="grey">Nombre</th>
            <th class="grey">Cantidad</th>
            <th class="grey">Precio</th>
        </tr>
            <?php foreach ($productos as $p) { ?> 
                <?php if (($i % 2) == 0) { ?>
                    <tr>
                        <td class="claro"><?php echo $productos[$i]->nombre ?></td>
                        <td class="claro"><?php echo $productos[$i]->cantidad ?></td>
                        <td class="claro"><?php echo $productos[$i]->precio ?></td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td class="oscuro"><?php echo $productos[$i]->nombre ?></td>
                        <td class="oscuro"><?php echo $productos[$i]->cantidad ?></td>
                        <td class="oscuro"><?php echo $productos[$i]->precio ?></td>
                    </tr> 
                <?php } ?>
                <?php  ++$i ?>
            <?php } ?>
        </table>
    </div>
</body>
</html>