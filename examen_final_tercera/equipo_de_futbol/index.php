<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
    <body>
        <?php     
              include_once 'models/jugadorClass.php';
              include_once 'models/equipoClass.php';

        
              $equipo1 = new EquipoFutbol("Chelsea");
              $equipo1->agregarJugador(new Jugador("Mathias Moreira", "Delantero", 10));
              $equipo1->agregarJugador(new Jugador("Roberto Carlos", "Defensa", 5));
              $equipo1->agregarJugador(new Jugador("Martin Messi", "Mediocampista", 8));
              $equipo1->agregarJugador(new Jugador("Luis Miguel", "Arquero", 9));
              
              $equipo2 = new EquipoFutbol("Barcelona");
              $equipo2->agregarJugador(new Jugador("Carlos Santan", "Delantero", 10));
              $equipo2->agregarJugador(new Jugador("Rene Descartes", "Defensa", 9));
              $equipo2->agregarJugador(new Jugador("Shakira Gonzalez", "Mediocampista", 45));
              $equipo2->agregarJugador(new Jugador("Jerry Tom", "Arquero", 75));
              
              $equipos = array($equipo1, $equipo2);
              
              foreach ($equipos as $equipo) {
                echo "Equipo: " . $equipo->getNombre() . "<br>";
                echo "Jugadores:";
                echo "<br>";
                foreach ($equipo->getJugadores() as $jugador) {
                  echo $jugador->getNombre() . ", Posición: " . $jugador->getPosicion() . ", Goles marcados: " . $jugador->getNroCasaca() . "<br>";
                }
                echo "<br>";
              }
        ?>
    </body>
</html>