<?php 
    class Jugador {
            private $nombre;
            private $posicion;
            private $nroCasaca;
            
            public function __construct($nombre, $posicion, $nroCasaca) {
                $this->nombre = $nombre;
                $this->posicion = $posicion;
                $this->nroCasaca = $nroCasaca;
            }
            
            public function getNombre() {
                return $this->nombre;
            }
            
            public function getPosicion() {
                return $this->posicion;
            }
            
            public function getNroCasaca() {
                return $this->nroCasaca;
            }
        }
?>
