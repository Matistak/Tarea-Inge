<?php 
    class EquipoFutbol {
        private $nombre;
        private $jugadores = array();
        
        public function __construct($nombre) {
            $this->nombre = $nombre;
        }
        
        public function agregarJugador(Jugador $jugador) {
            $this->jugadores[] = $jugador;
        }
        
        public function getNombre() {
            return $this->nombre;
        }
        
        public function getJugadores() {
            return $this->jugadores;
        }
    }
?>